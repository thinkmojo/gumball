 // Flash Files (LittleFS)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <FS.h>
#include <LittleFS.h>

// Deploy the fleet (DTF)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <DTF_ESP8266Update.h> 


// Wifi + Dweet
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>               


//for LED status
#include <Ticker.h>
Ticker ticker;

#ifndef LED_BUILTIN
#define LED_BUILTIN 13 // ESP32 DOES NOT DEFINE LED_BUILTIN
#endif

// wifimanager
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <ESP8266WiFi.h>          //ESP8266 Core WiFi Library (you most likely already have this in your sketch)
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic


// esp8266audio + MAX98357A
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <AudioFileSourceLittleFS.h>
#include <AudioFileSourceID3.h>
#include <AudioGeneratorMP3.h>
#include <AudioOutputI2S.h>

AudioGeneratorMP3 *mp3;
AudioFileSourceLittleFS *file;
AudioOutputI2S *out;

// FSBrowser
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define INCLUDE_FALLBACK_INDEX_HTM // keep index in binary rather than littleFS
#include "extras/index_htm.h" // keep index in binary rather than littleF



#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h> // check potential conflict with <DNSServer.h>
#include <SPI.h>
#include <WiFiClient.h>


const char* fsName = "LittleFS";
FS* fileSystem = &LittleFS;
LittleFSConfig fileSystemConfig = LittleFSConfig();
static bool fsOK;
String unsupportedFiles = String();
File uploadFile;
static const char TEXT_PLAIN[] PROGMEM = "text/plain";
static const char FS_INIT_ERROR[] PROGMEM = "FS INIT ERROR";
static const char FILE_NOT_FOUND[] PROGMEM = "FileNotFound";
static bool checkedForUpdates = false;

const char* host = "gumball"; // USed to define webserver DNS -> "http://gumball.local/"
ESP8266WebServer server(80);




// Global Variables
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
String PRODUCT_NAME = "gumball";
String ESP_NAME = String(ESP.getChipId(), HEX); // Generate unique ESP ID
String DEVICE_FULL_NAME = String(PRODUCT_NAME) + '_' + String(ESP_NAME);
String CURRENT_FIRMWARE_VERSION ="1.0.6"; // DTF
/* ADD ESP UNIQUE ID TO WIFI NAME -> 
 https://stackoverflow.com/questions/40033125/how-to-pass-a-variable-to-wifimanager-autoconnect-to-name-the-ap
*/

WiFiManager wifiManager; // Moved to global because we need to invoke this variable in the loop when there is a trigger to reset settings

int timeout = 120; // seconds to run for

volatile bool playing = 0;



// Define pin -> 5 = D1 on Wemos
// https://escapequotes.net/esp8266-wemos-d1-mini-pins-and-diagram/ 
const int Relay = 5; 
int LED = LED_BUILTIN;

// Dweet
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//String answer = "off";// Dweet default (reset) answer
int httpCode = 0;
HTTPClient http;
String thing = String(DEVICE_FULL_NAME);
String key = "trigger";
String triggerCode = "0";



void tick()
{
  //toggle state
  digitalWrite(LED, !digitalRead(LED));     // set pin to the opposite state
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
   file = new AudioFileSourceLittleFS("/setup/system_error.mp3"); // default file played when connected
    if (mp3->begin(file, out)) {
        Serial.println("WiFi Disconnected");
    }
}



//############################################################################
// SETUP
//############################################################################

void setup() {

  // Start Serial
  Serial.begin(115200);
  delay(10);
  Serial.println('\n');

  // SET VARIABLES
  pinMode(Relay, OUTPUT); // ini relay
  //set led pin as output
  pinMode(LED, OUTPUT);


/*
  // -------- TEMPORARY RX/TX BUG SWAP ------------------------------
  // Make sure to disable this in order to upload files to littleFS
  // and to be able to see the serial monitor

  pinMode(1, INPUT); // 1. Add this line to the code
                     // 2. Solder both RX and TX pins together
  // -----------------------------------------------------------------
*/

  
  // check firmware version
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 // delay(500);
  Serial.println("--------");
  Serial.print(DEVICE_FULL_NAME);
  Serial.println(" - version: " + CURRENT_FIRMWARE_VERSION);// DTF - Deploy the fleet
  checkedForUpdates = false; 
  
  // INIT LittleFS
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Serial.println("Mount LittleFS"); // LittleFS
  if (!LittleFS.begin()) {Serial.println("LittleFS mount failed");return;}



  // INIT ESP8266Audio
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Serial.println("Audio Setup");
  file = NULL;
  out = new AudioOutputI2S();
  out->SetOutputModeMono(true);
  out->SetGain(2.0); // set volume from 0.0 to 4.0
  mp3 = new AudioGeneratorMP3();
  randomSeed(analogRead(0)); // Generate random seed for random mp3
  

  // Initiate WIFImanager
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
  
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  
  //first parameter is name of access point, second is the password
  //wifiManager.autoConnect("GUMBALL", "GUM12345"); // Password has to be 8 characters minimum
  wifiManager.autoConnect((const char*)DEVICE_FULL_NAME.c_str(),"GUM12345");
    
  // Uncomment and run it once, if you want to erase all the stored information
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
   wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.restart();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  ticker.detach();
  //keep LED off
  digitalWrite(LED, LOW);
  digitalWrite(Relay, LOW);

  //  File System Init
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  fileSystemConfig.setAutoFormat(false);
  fileSystem->setConfig(fileSystemConfig);
  fsOK = fileSystem->begin();
  Serial.println(fsOK ? F("Filesystem initialized.") : F("Filesystem init failed!"));

 

  //  MDNS Init
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  if (MDNS.begin(host)) {
    MDNS.addService("http", "tcp", 80);
    Serial.print(F("Open http://"));
    Serial.print(host);
    Serial.println(F(".local/edit to open the FileSystem Browser"));
  }


//  //  Web Server Init
//  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  server.on("/status", HTTP_GET, handleStatus); // Filesystem status
  server.on("/list", HTTP_GET, handleFileList); // List directory
  server.on("/edit", HTTP_GET, handleGetEdit); // Load editor
  server.on("/edit",  HTTP_PUT, handleFileCreate); // Create file
  server.on("/edit",  HTTP_DELETE, handleFileDelete); // Delete file
//  // Upload file
//  // - first callback is called after the request has ended with all parsed arguments
//  // - second callback handles file upload at that location
  server.on("/edit",  HTTP_POST, replyOK, handleFileUpload);
//
//  // Default handler for all URIs not defined above
//  // Use it to read files from filesystem
  server.onNotFound(handleNotFound);
//
  // Start web server
  server.begin();
  Serial.println("HTTP server started");
  
  // Connected to wifi
  file = new AudioFileSourceLittleFS("/setup/system_success.mp3"); // default file played when connected
  if (mp3->begin(file, out)) {
        Serial.println("playing internet success mp3:");
    }




} // END setup()


//############################################################################
// LOOP
//############################################################################

void loop() {
 
 
  // HANDLE WEB SERVER
  server.handleClient();
  MDNS.update();

 // Serial.print("WiFi Status Code:");
 // Serial.println(WiFi.status());


  if (mp3->isRunning()) {
     digitalWrite(Relay, HIGH);
    if (!mp3->loop()) {
      uint32_t e = millis();
      mp3->stop();
       digitalWrite(Relay, LOW);
    }
  } 
  
  
  else {
  // GET ANSWER FROM THE DWEET

while (WiFi.status() != WL_CONNECTED) {  // Handle for wifi disconnect 

    file = new AudioFileSourceLittleFS("/setup/system_error.mp3"); // default file played when connected
    if (mp3->begin(file, out)) {
        Serial.println("WiFi Disconnected");
    }
//     Serial.println ("WiFi Disconnected");
  }
  
  if (WiFi.status() == WL_CONNECTED) { 
     getDweetcode (); 
    
    }
  

  if (triggerCode > "0" && triggerCode <= "20" ) {
 
    playAudio ();

 
    } 
// DTF firmware update
  else if(triggerCode == "30") { 
    
                    
                  //  file = new AudioFileSourceLittleFS("/setup/system_success.mp3"); // replace with firmware_update.mp3

//                    Serial.println("[FIRMWARE UPDATE] -> Updating firmware " + CURRENT_FIRMWARE_VERSION);
//                    DTF_ESP8266Update::getFirmwareUpdate("https://ota.deploythefleet.io/d317dbf6-ebf4-431f-a1c1-3f6bae8292e1", CURRENT_FIRMWARE_VERSION.c_str());
//                    delay (5000);
//                    Serial.println("[FIRMWARE UPDATE] -> Succesfully updated to: " + CURRENT_FIRMWARE_VERSION);
//                    delay(1000);
                   
 
  Serial.println("Checking for Updates");
  // wait for WiFi connection
  if ((WiFi.status() == WL_CONNECTED)) {
    if (!checkedForUpdates)
    {
      Serial.println("Updating");
      checkedForUpdates = true;
      DTF_ESP8266Update::getFirmwareUpdate("https://ota.deploythefleet.io/d317dbf6-ebf4-431f-a1c1-3f6bae8292e1", CURRENT_FIRMWARE_VERSION.c_str());
      Serial.println("Update Completed");
    }
    Serial.println("This is version " + CURRENT_FIRMWARE_VERSION);
    delay(5000);
  }
 triggerCode = "0";
                }

                
// IF RESET WIFI CONNECTION
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                
  else if(triggerCode == "40") {
                    playAudio ();

                        
                    Serial.println("[INTERNET WIFI] -> Resetting... ");
                    wifiManager.resetSettings();
                    Serial.println("[INTERNET WIFI] -> Reset succesfuly");
                    delay(1000);
                    Serial.println("[INTERNET WIFI] -> Restarting ESP");
                    delay(1000);
                    ESP.restart(); 
//                    triggerCode = "0";                   
                        

                    
     }

  }
   
} // END loop()



//############################################################################
// RESET DWEET TRIGGER
//############################################################################

void setKeyToFalse(){ // Reset the key from the dweet to its initial state
  http.begin("http://dweet.io/dweet/for/" + thing + "?" + String(key)+ "=off");
  http.addHeader("Content-Type","text/plain");
  httpCode = http.GET();
  Serial.println(httpCode);
  Serial.println("Value has been set to: off for" + thing);
  http.end();
}

//############################################################################
// GET DWEET TRIGGER
//############################################################################
void getDweetcode () {

  http.begin("http://dweet.io/listen/for/dweets/from/" + thing);     // Listen api 
           
  httpCode = http.GET();                                                           
  Serial.print("HTTP CODE: ");
  Serial.println(httpCode);
  
  
  if (httpCode == 200){
      String response = http.getString();  
     // Serial.println(response);
      int sstart = response.indexOf("trigger");  // Find the word Trigger in response
      triggerCode = response.substring(sstart+10,sstart+12);  //Extract the Trigger code 
      Serial.println("Trigger Code is:" + triggerCode);
      }      
  http.end();   

}




void playAudio () {
  Serial.println("Initialising Audio");
  delete file;

  
  if(triggerCode == "10") file = new AudioFileSourceLittleFS("/0.mp3");
    else if(triggerCode == "11") file = new AudioFileSourceLittleFS("/1.mp3");
    else if(triggerCode == "12") file = new AudioFileSourceLittleFS("/2.mp3");
    else if(triggerCode == "13") file = new AudioFileSourceLittleFS("/3.mp3");
    else if(triggerCode == "14") file = new AudioFileSourceLittleFS("/4.mp3");
    else if(triggerCode == "15") file = new AudioFileSourceLittleFS("/5.mp3");
    else if(triggerCode == "16") file = new AudioFileSourceLittleFS("/6.mp3");
    else if(triggerCode == "17") file = new AudioFileSourceLittleFS("/7.mp3");
    else if(triggerCode == "18") file = new AudioFileSourceLittleFS("/8.mp3");
    else if(triggerCode == "19") file = new AudioFileSourceLittleFS("/9.mp3");
    else if(triggerCode == "40") file = new AudioFileSourceLittleFS("/setup/system_notif.mp3"); // replace with reset_wifi.mp3
 //Issue with triigger 40 - not playing sound when using system_notif    
  
  mp3 -> begin(file,out); //Start playing the track loaded
  Serial.print("playing mp3:");
  Serial.println(triggerCode);
  triggerCode = "0";

}



//############################################################################
// FSBROWSER FUNCTIONS
//############################################################################

///////////////////////////////////////////////////////////////
// Utils to return HTTP codes, and determine content-type

void replyOK() {
  server.send(200, FPSTR(TEXT_PLAIN), "");
}

void replyOKWithMsg(String msg) {
  server.send(200, FPSTR(TEXT_PLAIN), msg);
}

void replyNotFound(String msg) {
  server.send(404, FPSTR(TEXT_PLAIN), msg);
}

void replyBadRequest(String msg) {
  Serial.println(msg);
  server.send(400, FPSTR(TEXT_PLAIN), msg + "\r\n");
}

void replyServerError(String msg) {
  Serial.println(msg);
  server.send(500, FPSTR(TEXT_PLAIN), msg + "\r\n");
}


////////////////////////////////
// Request handlers

/*
   Return the FS type, status and size info
*/
void handleStatus() {
  Serial.println("handleStatus");
  FSInfo fs_info;
  String json;
  json.reserve(128);

  json = "{\"type\":\"";
  json += fsName;
  json += "\", \"isOk\":";
  if (fsOK) {
    fileSystem->info(fs_info);
    json += F("\"true\", \"totalBytes\":\"");
    json += fs_info.totalBytes;
    json += F("\", \"usedBytes\":\"");
    json += fs_info.usedBytes;
    json += "\"";
  } else {
    json += "\"false\"";
  }
  json += F(",\"unsupportedFiles\":\"");
  json += unsupportedFiles;
  json += "\"}";

  server.send(200, "application/json", json);
}


/*
   Return the list of files in the directory specified by the "dir" query string parameter.
   Also demonstrates the use of chuncked responses.
*/
void handleFileList() {
  if (!fsOK) {
    return replyServerError(FPSTR(FS_INIT_ERROR));
  }

  if (!server.hasArg("dir")) {
    return replyBadRequest(F("DIR ARG MISSING"));
  }

  String path = server.arg("dir");
  if (path != "/" && !fileSystem->exists(path)) {
    return replyBadRequest("BAD PATH");
  }

  Serial.println(String("handleFileList: ") + path);
  Dir dir = fileSystem->openDir(path);
  path.clear();

  // use HTTP/1.1 Chunked response to avoid building a huge temporary string
  if (!server.chunkedResponseModeStart(200, "text/json")) {
    server.send(505, F("text/html"), F("HTTP1.1 required"));
    return;
  }

  // use the same string for every line
  String output;
  output.reserve(64);
  while (dir.next()) {
#ifdef USE_SPIFFS
    String error = checkForUnsupportedPath(dir.fileName());
    if (error.length() > 0) {
      Serial.println(String("Ignoring ") + error + dir.fileName());
      continue;
    }
#endif
    if (output.length()) {
      // send string from previous iteration
      // as an HTTP chunk
      server.sendContent(output);
      output = ',';
    } else {
      output = '[';
    }

    output += "{\"type\":\"";
    if (dir.isDirectory()) {
      output += "dir";
    } else {
      output += F("file\",\"size\":\"");
      output += dir.fileSize();
    }

    output += F("\",\"name\":\"");
    // Always return names without leading "/"
    if (dir.fileName()[0] == '/') {
      output += &(dir.fileName()[1]);
    } else {
      output += dir.fileName();
    }

    output += "\"}";
  }

  // send last string
  output += "]";
  server.sendContent(output);
  server.chunkedResponseFinalize();
}


/*
   Read the given file from the filesystem and stream it back to the client
*/
bool handleFileRead(String path) {
  Serial.println(String("handleFileRead: ") + path);
  if (!fsOK) {
    replyServerError(FPSTR(FS_INIT_ERROR));
    return true;
  }

  if (path.endsWith("/")) {
    path += "index.htm";
  }

  String contentType;
  if (server.hasArg("download")) {
    contentType = F("application/octet-stream");
  } else {
    contentType = mime::getContentType(path);
  }

  if (!fileSystem->exists(path)) {
    // File not found, try gzip version
    path = path + ".gz";
  }
  if (fileSystem->exists(path)) {
    File file = fileSystem->open(path, "r");
    if (server.streamFile(file, contentType) != file.size()) {
      Serial.println("Sent less data than expected!");
    }
    file.close();
    return true;
  }

  return false;
}


/*
   As some FS (e.g. LittleFS) delete the parent folder when the last child has been removed,
   return the path of the closest parent still existing
*/
String lastExistingParent(String path) {
  while (!path.isEmpty() && !fileSystem->exists(path)) {
    if (path.lastIndexOf('/') > 0) {
      path = path.substring(0, path.lastIndexOf('/'));
    } else {
      path = String();  // No slash => the top folder does not exist
    }
  }
  Serial.println(String("Last existing parent: ") + path);
  return path;
}

/*
   Handle the creation/rename of a new file
   Operation      | req.responseText
   ---------------+--------------------------------------------------------------
   Create file    | parent of created file
   Create folder  | parent of created folder
   Rename file    | parent of source file
   Move file      | parent of source file, or remaining ancestor
   Rename folder  | parent of source folder
   Move folder    | parent of source folder, or remaining ancestor
*/
void handleFileCreate() {
  if (!fsOK) {
    return replyServerError(FPSTR(FS_INIT_ERROR));
  }

  String path = server.arg("path");
  if (path.isEmpty()) {
    return replyBadRequest(F("PATH ARG MISSING"));
  }

#ifdef USE_SPIFFS
  if (checkForUnsupportedPath(path).length() > 0) {
    return replyServerError(F("INVALID FILENAME"));
  }
#endif

  if (path == "/") {
    return replyBadRequest("BAD PATH");
  }
  if (fileSystem->exists(path)) {
    return replyBadRequest(F("PATH FILE EXISTS"));
  }

  String src = server.arg("src");
  if (src.isEmpty()) {
    // No source specified: creation
    Serial.println(String("handleFileCreate: ") + path);
    if (path.endsWith("/")) {
      // Create a folder
      path.remove(path.length() - 1);
      if (!fileSystem->mkdir(path)) {
        return replyServerError(F("MKDIR FAILED"));
      }
    } else {
      // Create a file
      File file = fileSystem->open(path, "w");
      if (file) {
        file.write((const char *)0);
        file.close();
      } else {
        return replyServerError(F("CREATE FAILED"));
      }
    }
    if (path.lastIndexOf('/') > -1) {
      path = path.substring(0, path.lastIndexOf('/'));
    }
    replyOKWithMsg(path);
  } else {
    // Source specified: rename
    if (src == "/") {
      return replyBadRequest("BAD SRC");
    }
    if (!fileSystem->exists(src)) {
      return replyBadRequest(F("SRC FILE NOT FOUND"));
    }

    Serial.println(String("handleFileCreate: ") + path + " from " + src);

    if (path.endsWith("/")) {
      path.remove(path.length() - 1);
    }
    if (src.endsWith("/")) {
      src.remove(src.length() - 1);
    }
    if (!fileSystem->rename(src, path)) {
      return replyServerError(F("RENAME FAILED"));
    }
    replyOKWithMsg(lastExistingParent(src));
  }
}


/*
   Delete the file or folder designed by the given path.
   If it's a file, delete it.
   If it's a folder, delete all nested contents first then the folder itself

   IMPORTANT NOTE: using recursion is generally not recommended on embedded devices and can lead to crashes (stack overflow errors).
   This use is just for demonstration purpose, and FSBrowser might crash in case of deeply nested filesystems.
   Please don't do this on a production system.
*/
void deleteRecursive(String path) {
  File file = fileSystem->open(path, "r");
  bool isDir = file.isDirectory();
  file.close();

  // If it's a plain file, delete it
  if (!isDir) {
    fileSystem->remove(path);
    return;
  }

  // Otherwise delete its contents first
  Dir dir = fileSystem->openDir(path);

  while (dir.next()) {
    deleteRecursive(path + '/' + dir.fileName());
  }

  // Then delete the folder itself
  fileSystem->rmdir(path);
}


/*
   Handle a file deletion request
   Operation      | req.responseText
   ---------------+--------------------------------------------------------------
   Delete file    | parent of deleted file, or remaining ancestor
   Delete folder  | parent of deleted folder, or remaining ancestor
*/
void handleFileDelete() {
  if (!fsOK) {
    return replyServerError(FPSTR(FS_INIT_ERROR));
  }

  String path = server.arg(0);
  if (path.isEmpty() || path == "/") {
    return replyBadRequest("BAD PATH");
  }

  Serial.println(String("handleFileDelete: ") + path);
  if (!fileSystem->exists(path)) {
    return replyNotFound(FPSTR(FILE_NOT_FOUND));
  }
  deleteRecursive(path);

  replyOKWithMsg(lastExistingParent(path));
}

/*
   Handle a file upload request
*/
void handleFileUpload() {
  if (!fsOK) {
    return replyServerError(FPSTR(FS_INIT_ERROR));
  }
  if (server.uri() != "/edit") {
    return;
  }
  HTTPUpload& upload = server.upload();
  if (upload.status == UPLOAD_FILE_START) {
    String filename = upload.filename;
    // Make sure paths always start with "/"
    if (!filename.startsWith("/")) {
      filename = "/" + filename;
    }
    Serial.println(String("handleFileUpload Name: ") + filename);
    uploadFile = fileSystem->open(filename, "w");
    if (!uploadFile) {
      return replyServerError(F("CREATE FAILED"));
    }
    Serial.println(String("Upload: START, filename: ") + filename);
  } else if (upload.status == UPLOAD_FILE_WRITE) {
    if (uploadFile) {
      size_t bytesWritten = uploadFile.write(upload.buf, upload.currentSize);
      if (bytesWritten != upload.currentSize) {
        return replyServerError(F("WRITE FAILED"));
      }
    }
    Serial.println(String("Upload: WRITE, Bytes: ") + upload.currentSize);
  } else if (upload.status == UPLOAD_FILE_END) {
    if (uploadFile) {
      uploadFile.close();
    }
    Serial.println(String("Upload: END, Size: ") + upload.totalSize);
  }
}


/*
   The "Not Found" handler catches all URI not explicitely declared in code
   First try to find and return the requested file from the filesystem,
   and if it fails, return a 404 page with debug information
*/
void handleNotFound() {
  if (!fsOK) {
    return replyServerError(FPSTR(FS_INIT_ERROR));
  }

  String uri = ESP8266WebServer::urlDecode(server.uri()); // required to read paths with blanks

  if (handleFileRead(uri)) {
    return;
  }

  // Dump debug data
  String message;
  message.reserve(100);
  message = F("Error: File not found\n\nURI: ");
  message += uri;
  message += F("\nMethod: ");
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += F("\nArguments: ");
  message += server.args();
  message += '\n';
  for (uint8_t i = 0; i < server.args(); i++) {
    message += F(" NAME:");
    message += server.argName(i);
    message += F("\n VALUE:");
    message += server.arg(i);
    message += '\n';
  }
  message += "path=";
  message += server.arg("path");
  message += '\n';
  Serial.print(message);

  return replyNotFound(message);
}

/*
   This specific handler returns the index.htm (or a gzipped version) from the /edit folder.
   If the file is not present but the flag INCLUDE_FALLBACK_INDEX_HTM has been set, falls back to the version
   embedded in the program code.
   Otherwise, fails with a 404 page with debug information
*/
void handleGetEdit() {
  if (handleFileRead(F("/edit/index.htm"))) {
    return;
  }

#ifdef INCLUDE_FALLBACK_INDEX_HTM
  server.sendHeader(F("Content-Encoding"), "gzip");
  server.send(200, "text/html", index_htm_gz, index_htm_gz_len);
#else
  replyNotFound(FPSTR(FILE_NOT_FOUND));
#endif

}

//############################################################################
// END FSBROWSER FUNCTIONS
//############################################################################
